const express = require("express");
const mongoose = require("mongoose");
const passport = require("passport");

require("dotenv").config();

const dbConfig = require("./config/database");
const users = require("./users");
const profile = require("./profile");
const posts = require("./posts");

const app = express();
app.use(express.urlencoded({ extended: false }));
app.use(express.json());

mongoose
  .connect(dbConfig.mongoURI, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => console.log("MongoDB Connected"))
  .catch((err) => console.log(err));

const authorize = require("./config/passport");
authorize(passport);
app.use(passport.initialize());

const port = process.env.PORT || 5000;
app.get("/ping", (request, response) => {
  response.json({ data: request.body, message: "Pong" });
});

app.use("/api/users", users);
app.use("/api/profile", profile);
app.use("/api/posts", posts);

app.listen(port, () => {
  console.log(`The Service has started on port ${port}`);
});
