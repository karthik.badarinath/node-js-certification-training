const bcrypt = require("bcrypt");
const express = require("express");
const jwt = require("jsonwebtoken");
const passport = require("passport");

const jwtConfig = require("./config/jwt");
const validate = require("./validation/register");
const validateLogin = require("./validation/login");

const User = require("./model/UserModel");

const router = express.Router();

router.post("/register", async (req, res) => {
  const result = validate(req.body);
  if (result.hasError) {
    return res.status(400).json({ data: req.body, result: result });
  }

  if (await User.alreadyExists(req.body.email)) {
    return res.status(400).json({
      data: req.body,
      result: { hasError: true, errors: { email: "Email already exists!" } },
    });
  }

  const user = new User({
    name: req.body.name,
    email: req.body.email,
    password: req.body.password,
  });

  bcrypt.genSalt(10, (_, salt) => {
    bcrypt.hash(user.password, salt, async (error, hash) => {
      if (error) {
        throw error;
      }

      let hasError = false;
      let status = 201;
      let errors = {};
      let response = {
        id: null,
        name: req.body.name,
        email: req.body.email,
      };

      try {
        user.password = hash;
        response.id = await (await user.save())._id;
      } catch (e) {
        hasError = true;
        status = 400;
        errors = e;
      }

      res.status(status).json({
        data: response,
        result: { hasError: hasError, errors: errors },
      });
    });
  });
});

router.post("/login", async (req, res) => {
  const result = validateLogin(req.body);
  if (result.hasError) {
    return res.status(400).json({ data: req.body, result: result });
  }

  const user = await User.findByEmail(req.body.email);
  if (
    !(await User.alreadyExists(req.body.email)) ||
    !(await bcrypt.compare(req.body.password, user.password))
  ) {
    return res.status(400).json({
      data: req.body,
      result: {
        hasError: true,
        error: { message: "Invalid email address or password!" },
      },
    });
  }

  jwt.sign(
    { id: user._id, name: user.name, email: user.email },
    jwtConfig.secret,
    { expiresIn: jwtConfig.expiry },
    (error, token) => {
      const status = error === null ? 200 : 400;

      return res.status(status).json({
        access: { token, expiry: jwtConfig.expiry },
        result: { hasError: error !== null, errors: error },
      });
    }
  );
});

router.get(
  "/info",
  passport.authenticate("jwt", { session: false }),
  async (req, res) => {
    const token = req.headers.authorization.split(" ")[1];
    jwt.verify(token, jwtConfig.secret, async (error, user) => {
      if (error !== null) {
        return res.status(401).json({
          data: {},
          errors: {
            hasError: true,
            message: error,
          },
        });
      }

      let status = 200;
      let response = {
        data: {},
        errors: {
          hasError: false,
          message: null,
        },
      };

      try {
        let data = await User.get(user.id);
        response.data.id = data._id;
        response.data.name = data.name;
        response.data.email = data.email;
        response.data.createdAt = data.createdAt;
      } catch (e) {
        response.errors.hasError = true;
        response.errors.message = `Unable to retreive user data for '${user.id}'`;
        status = 400;
      }

      res.status(status).json(response);
    });
  }
);

module.exports = router;
