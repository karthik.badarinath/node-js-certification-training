const passportJwt = require("passport-jwt");
//const mongoose = require("mongoose");

const User = require("../model/UserModel");
//const User = mongoose.model("users");
const jwtConfig = require("./jwt");

const JwtStrategy = passportJwt.Strategy;
const ExtractJwt = passportJwt.ExtractJwt;

const options = {
  secretOrKey: jwtConfig.secret,
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
};

const strategy = new JwtStrategy(options, async (payload, done) => {
  try {
    const user = await User.get(payload.id);

    return user ? done(null, user) : done(null, false);
  } catch (e) {
    return done(e, null);
  }
});

module.exports = (passport) => passport.use(strategy);
