module.exports = {
  secret: `${process.env.SECRET}`,
  expiry: `${process.env.EXPIRY}`,
};
