const validator = require("validator");
const isEmpty = require("./is-empty");

const validate = (data) => {
  let error = {};

  data.email = !isEmpty(data.email) ? data.email : "";
  data.password = !isEmpty(data.password) ? data.password : "";

  if (!validator.isEmail(data.email)) {
    error.email = `Email address is invalid!`;
  }

  Object.keys(data).forEach((key) => {
    if (validator.isEmpty(data[key])) {
      error[key] = `The key '${key}' cannot be empty`;
    }
  });

  return {
    hasError: !isEmpty(error),
    errors: error,
  };
};

module.exports = validate;
