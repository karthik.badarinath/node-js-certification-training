const validator = require("validator");
const isEmpty = require("./is-empty");

const validate = (data) => {
  let error = {};

  data.name = !isEmpty(data.name) ? data.name : "";
  data.email = !isEmpty(data.email) ? data.email : "";
  data.password = !isEmpty(data.password) ? data.password : "";
  data.repeatPassword = !isEmpty(data.repeatPassword)
    ? data.repeatPassword
    : "";

  if (!validator.isLength(data.name, { min: 2, max: 30 })) {
    error.name = `Name must be between 2 and 30 character long, ${data.name.length} given`;
  }

  if (!validator.isLength(data.password, { min: 6, max: 30 })) {
    error.password = `Password must be at least 6 character long, ${data.password.length} given`;
  }

  if (!validator.isEmail(data.email)) {
    error.email = `Email address is invalid!`;
  }

  if (!validator.equals(data.password, data.repeatPassword)) {
    error.repeatPassword = "Repeat password did not match your password";
  }

  Object.keys(data).forEach((key) => {
    if (validator.isEmpty(data[key])) {
      error[key] = `The key '${key}' cannot be empty`;
    }
  });

  return {
    hasError: !isEmpty(error),
    errors: error,
  };
};

module.exports = validate;
