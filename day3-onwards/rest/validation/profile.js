const validator = require("validator");
const isEmpty = require("./is-empty");

module.exports = function validateProfileInput(data) {
  let errors = {};
  data.handle = !isEmpty(data.handle) ? data.handle : "";
  data.skills = !isEmpty(data.skills) ? data.skills : "";
  data.status = !isEmpty(data.status) ? data.status : "";

  const requiredFields = ["handle", "skills", "status"];
  const urls = [
    "facebook",
    "instagram",
    "linkedin",
    "twitter",
    "website",
    "youtube",
  ];

  if (!validator.isLength(data.handle, { min: 2, max: 40 })) {
    errors.handle = "Handle needs to between 2 and 4 characters";
  }

  requiredFields.forEach((key) => {
    if (validator.isEmpty(data[key])) {
      errors[key] = `The key '${key}' is required`;
    }
  });

  urls.forEach((key) => {
    if (!isEmpty(data[key]) && !validator.isURL(data[key])) {
      errors[key] = `The key '${key}' does not have a valid URL`;
    }
  });

  return {
    isValid: isEmpty(errors),
    errors,
  };
};
