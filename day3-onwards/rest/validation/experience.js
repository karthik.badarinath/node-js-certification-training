const validator = require("validator");
const isEmpty = require("./is-empty");

module.exports = function validateExperienceInput(data) {
  let errors = {};
  data.title = !isEmpty(data.title) ? data.title : "";
  data.company = !isEmpty(data.company) ? data.company : "";
  data.from = !isEmpty(data.from) ? data.from : "";

  const requiredFields = ["title", "company", "from"];

  if (!isEmpty(data.to) && !validator.isAfter(data.to, data.from)) {
    errors.to = `The 'to' date selected must be greater than '${data.from}' date`;
  }

  requiredFields.forEach((key) => {
    if (validator.isEmpty(data[key])) {
      errors[key] = `The key '${key}' is required`;
    }
  });

  return {
    isValid: isEmpty(errors),
    errors,
  };
};
