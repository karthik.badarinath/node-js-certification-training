const express = require("express");
const passport = require("passport");

const router = express.Router();
const validate = require("./validation/profile");
const validateExperience = require("./validation/experience");
const Profile = require("./model/ProfileModel");
const experience = require("./validation/experience");

router.post(
  "/",
  passport.authenticate("jwt", { session: false }),
  async (req, res) => {
    const result = validate(req.body);
    if (!result.isValid) {
      return res.status(400).json({ data: null, ...result });
    }

    let profileFields = {
      bio: req.body.bio ?? "",
      company: req.body.company ?? "",
      education: [],
      experience: [],
      githubusername: req.body.githubusername ?? "",
      handle: req.body.handle ?? "",
      location: req.body.location ?? "",
      skills: req.body.skills !== "undefined" ? req.body.skills.split(",") : [],
      social: {},
      status: req.body.status ?? "",
      user: req.user.id,
      website: req.body.website ?? "",
    };

    if (req.body.facebook) profileFields.social.facebook = req.body.facebook;
    if (req.body.instagram) profileFields.social.instagram = req.body.instagram;
    if (req.body.linkedin) profileFields.social.linkedin = req.body.linkedin;
    if (req.body.twitter) profileFields.social.twitter = req.body.twitter;
    if (req.body.youtube) profileFields.social.youtube = req.body.youtube;

    const profile = await Profile.findOne({ user: req.user._id });
    if (profile) {
      const updatedProfile = await Profile.findOneAndUpdate(
        { user: req.user._id },
        { $set: profileFields },
        { new: true }
      );

      let status = 200;
      let isValid = true;
      if (updatedProfile._id === "undefined") {
        isValid = false;
        status = 400;
      }

      return res.status(status).json({
        data: updatedProfile,
        isValid: isValid,
        errors: null,
      });
    }

    /*const handle = await Profile.findOne({ handle: profileFields.handle });
    if (handle) {
      return res
        .status(400)
        .json({ data: null, isValid: false, errors: "Handle already exists!" });
    }*/

    const newProfile = await new Profile(profileFields).save();
    let status = 201;
    let isValid = true;
    if (newProfile._id === "undefined") {
      isValid = false;
      status = 400;
    }

    return res.status(status).json({
      data: newProfile,
      isValid: isValid,
      errors: null,
    });
  }
);

router.get(
  "/",
  passport.authenticate("jwt", { session: false }),
  async (req, res) => {
    try {
      const profile = await Profile.findOne({
        user: req.user.id,
      });

      if (profile !== null) {
        return res.status(200).json({ profile, error: null });
      }

      return res
        .status(400)
        .json({ profile: null, error: "Profile not found!" });
    } catch (errors) {
      return res
        .status(400)
        .json({ profile: null, error: "Something went wrong" });
    }
  }
);

router.get("/all", async (req, res) => {
  try {
    const profiles = await Profile.find().populate("user", ["name", "email"]);

    if (profiles.length > 0) {
      return res.status(200).json({ profiles, error: null });
    }

    return res
      .status(400)
      .json({ profiles: null, error: "No profiles found!" });
  } catch (_) {
    return res
      .status(400)
      .json({ profile: null, error: "Something went wrong" });
  }
});

router.get(
  "/handle/:handle",
  passport.authenticate("jwt", { session: false }),
  async (req, res) => {
    try {
      const profile = await Profile.findOne({
        handle: req.params.handle,
      });

      if (profile !== null) {
        return res.status(200).json({ profile, error: null });
      }

      return res
        .status(400)
        .json({ profile: null, error: "Profile not found!" });
    } catch (_) {
      return res
        .status(400)
        .json({ profile: null, error: "Something went wrong" });
    }
  }
);

router.get("/user/:id", async (req, res) => {
  try {
    const profile = await Profile.findOne({
      user: req.params.id,
    });

    if (profile !== null) {
      return res.status(200).json({ profile, error: null });
    }

    return res.status(400).json({ profile: null, error: "Profile not found!" });
  } catch (_) {
    return res
      .status(400)
      .json({ profile: null, error: "Something went wrong" });
  }
});

router.post(
  "/experience",
  passport.authenticate("jwt", { session: false }),
  async (req, res) => {
    try {
      const result = validateExperience(req.body);
      if (!result.isValid) {
        return res.status(400).json({ data: null, ...result });
      }

      const profile = await Profile.findOne({ user: req.user.id });

      if (profile === null) {
        return res.status(400).json({ profile, error: "Profile not found!" });
      }

      const experience = {
        title: req.body.title,
        company: req.body.company,
        location: req.body.location,
        from: req.body.from,
        to: req.body.to,
        current: req.body.current,
        description: req.body.description,
      };

      profile.experience.unshift(experience);
      const updatedProfile = await profile.save();
      if (updatedProfile !== null) {
        return res.status(200).json({ profile: updatedProfile, error: null });
      }

      return res
        .status(400)
        .json({ profile: null, error: "Unable to save experience!" });
    } catch (_) {
      return res
        .status(400)
        .json({ profile: null, error: "Something went wrong" });
    }
  }
);

router.delete(
  "/experience/:id",
  passport.authenticate("jwt", { session: false }),
  async (req, res) => {
    try {
      const profile = await Profile.findOne({ user: req.user.id });
      if (profile === null) {
        return res
          .status(400)
          .json({ profile: nul, error: "Profile not found!" });
      }

      const index = profile.experience
        .map((experience) => experience._id == req.params.id)
        .indexOf(req.params.id);

      profile.experience.splice(index, 1);
      const result = await profile.save();

      return res.json({ result, error: null });
    } catch (e) {
      return res.status(400).json({ profile: null, error: e });
    }
  }
);

module.exports = router;
