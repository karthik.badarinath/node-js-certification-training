const mongoose = require("mongoose");

const Name = "users";
const Schema = mongoose.Schema;

const UserSchema = new Schema({
  name: {
    type: String,
    require: true,
  },
  email: {
    type: String,
    require: true,
  },
  password: {
    type: String,
    require: true,
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
});

UserSchema.statics.alreadyExists = (email) => {
  return User.countDocuments({ email: email }).exec();
};

UserSchema.statics.findByEmail = (email) => {
  return User.findOne({ email: email }).exec();
};

UserSchema.statics.get = (id) => {
  return User.findById(id).exec();
};

module.exports = User = mongoose.model(Name, UserSchema);
