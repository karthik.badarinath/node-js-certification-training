const chai = require("chai");
const assert = chai.assert;
const expect = chai.expect;

const numbers = [1, 2, 3, 4, 5];

assert.isArray(numbers, "is an array of number");
expect(numbers).to.be.an("array").that.include(1);
//assert.include(numbers, 6, "is an array of number");
assert.isNotArray(0, "is not an array");
