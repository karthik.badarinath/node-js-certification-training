const chai = require("chai");
const chaiHttp = require("chai-http");
const faker = require("faker");
const expect = chai.expect;

chai.use(chaiHttp);

describe("Authentication", () => {
  const password = faker.internet.password();
  const email = faker.internet.email();

  it("register the user", (done) => {
    chai
      .request("localhost:5000")
      .post("/api/users/register")
      .send({
        name: faker.name.findName(),
        email: email,
        password: password,
        repeatPassword: password,
      })
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.have.status(201);
        done();
      });
  });

  it("login the registered user", (done) => {
    chai
      .request("localhost:5000")
      .post("/api/users/login")
      .send({
        email: email,
        password: password,
      })
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.have.status(200);
        done();
      });
  });

  it("invalid username", (done) => {
    chai
      .request("localhost:5000")
      .post("/api/users/login")
      .send({
        email: faker.name.findName(),
        password: password,
      })
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.have.status(400);
        done();
      });
  });

  it("invalid password", (done) => {
    chai
      .request("localhost:5000")
      .post("/api/users/login")
      .send({
        email: email,
        password: faker.internet.password(),
      })
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.have.status(400);
        done();
      });
  });

  it("invalid username password", (done) => {
    chai
      .request("localhost:5000")
      .post("/api/users/login")
      .send({
        email: faker.internet.email(),
        password: faker.internet.password(),
      })
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.have.status(400);
        done();
      });
  });
});
