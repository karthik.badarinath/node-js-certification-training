const fs = require("fs");

fs.stat("./tmp/sample.txt", (err, stat) => {
  if (err) {
    console.error(err);
    return false;
  }

  console.log(stat);
  return true;
});
