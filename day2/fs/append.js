const fs = require("fs");

const tmpPath = "./tmp/read.txt";

fs.appendFileSync(
  tmpPath,
  "\nAdding a new line content to existing one",
  (err) => {
    let msg = err || "Content appended successfully!";

    console.log(msg);
  }
);

console.log(
  fs.readFile(tmpPath, (err, data) => {
    let msg = err || data.toString();
    console.log(msg);
  })
);
