const fs = require("fs");
const tmpPath = "./tmp/sample.txt";

fs.readFile(tmpPath, (error, data) => {
  console.log("Data: ", data.toString());
});

const bufferReadData = fs.readFileSync(tmpPath).toString();
console.log("Buffer Data: ", bufferReadData);
