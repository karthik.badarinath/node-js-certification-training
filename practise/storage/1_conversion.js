const jsonExample = {
  title: "Software Engineering",
  author: "Ian Sommerville",
};

jsonString = JSON.stringify(jsonExample);
console.log(jsonString);

jsonObject = JSON.parse(jsonString);
console.log(jsonObject);
