const fs = require("fs");

fs.readFile("./tmp/json.txt", (error, fileData) => {
  let data = {
    hasError: error !== null,
    asIs: fileData.toString(),
    parsed: JSON.parse(fileData.toString()),
  };

  if (data.hasError) {
    console.error(error);
    return false;
  }

  console.log("As is data: ", data.asIs);
  console.log("Parsed Data: ", data.parsed);
  console.log("Title: ", data.parsed.title);
});
