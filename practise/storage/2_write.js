const fs = require("fs");

let jsonExample = {
  title: "Software Engineering",
  author: "Ian Sommerville",
};

fs.writeFile("./tmp/json.txt", JSON.stringify(jsonExample), (error) => {
  msg = error || "Write to file is successfull!";
  console.log(msg);
});
