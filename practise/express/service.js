const env = require("dotenv");
const exp = require("express");

const app = exp();
env.config({
  path: "~/web/nodejs",
});

app.get("/", (req, res) => {
  console.log("Request: ", req);
  res.send("<h1>Welcome to ExpressJS</h1>");
});

app.listen(process.env.PORT);
