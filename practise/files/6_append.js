const fs = require("fs");

fs.appendFile(
  "./tmp/sample.txt",
  "\nAppending some more data to this file",
  (error) => {
    const msg = error || "Data appended to the file successfully!";

    console.log(msg);
  }
);
