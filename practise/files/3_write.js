const fs = require("fs");

fs.writeFile(
  "./tmp/sample.txt",
  "Something that I wanted to write",
  (error) => {
    const msg = error || "File saved successfully!";
    console.log(msg);
  }
);
