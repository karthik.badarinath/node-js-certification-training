const fs = require("fs");

fs.rename("./tmp/sample.txt", "./tmp/renamed_file.txt", (error) => {
  const msg = error || "File renamed successfully!";
  console.log(msg);
});
