const fs = require("fs");

fs.stat("./tmp/sample.txt", (error, stats) => {
  if (error) {
    console.log(error);

    return false;
  }

  console.log("Is File: ", stats.isFile());
  console.log("Is Directory: ", stats.isDirectory());
  console.log("Size: ", stats.size);

  return true;
});
